// Default const 
const { errorHandler, successHandler, validateRequest } = require('../middleware/globalMiddleware');
const { body, check } = require('express-validator');
const model = require('../models/index');
const { dynamicUpdateData, dynamicStoreData, dynamicCreateOrUpdate } = require('../middleware/CRUDMiddleware'); // <-- Require the middleware
const bcrypt = require('bcryptjs');

// Define the whitelist of allowed columns with aliases
const allowedColumns = [
    'role',
    'email',
    'username',
    'password',
    'name',
];

exports.validation = (method) => {
    switch (method) {
        case 'post': {
            return [
                body('name', 'name doesn`t exists').exists(),
                check('name', 'name is empty').notEmpty(),
                body('email', 'email doesn`t exists').exists(),
                check('email', 'email  is empty').notEmpty(),
            ]
        }
    }
}

// Display list of all Authors.
exports.get = async (req, res,next) => {
    try {
        const users = await model.User.findAll({
            include: [
                {
                    model: model.Role,
                    as: 'Role',
                    attributes: ['name'],
                }
            ]
        });
        if (users.length !== 0) {
            // Set response data and message in res.locals
            res.locals.data = users;
            res.locals.message = 'Success';
        } else {
            // Set response data and message in res.locals
            res.locals.message = 'Data is empty';
            res.locals.data = {};
        }

        // Call successHandler middleware function
        successHandler(req, res, next);
    } catch (err) {
        // Call errorHandler middleware function with error object
        console.log(err);
        errorHandler(err, req, res, next);
    }
};
 
exports.post = async (req, res, next) => {
    try {
        // Update the data using the dynamicStoreData middleware
        // await dynamicStoreData(model.User, allowedColumns)(req, res, next);
        const salt = await bcrypt.genSalt();
        const hashPassword = await bcrypt.hash(req.body.password, salt);
        // dynamic crete or update
        const defaultWhere = {
            email: req.body.email,
            username: req.body.username,
        };
        // dynamic req type (body, query, param)
        const data = {
            email: req.body.email,
            username: req.body.username,
            password: hashPassword,
            name: req.body.name,
            role: req.body.role
        };
        // function 
        await dynamicCreateOrUpdate(model.User, data, allowedColumns, defaultWhere)(req, res, next);
    } catch (err) {
        // console.log(err);
        errorHandler(err, req, res, next);
    }
}

exports.update = async (req, res, next) => {
    try {
        // Extract the ID of the data to update from the request parameters
        const id = req.params.id;
        // Update the data using the dynamicUpdateData middleware
        await dynamicUpdateData(model.User, allowedColumns)(req, res, next);
    } catch (err) {
        // Pass any errors to the error handling middleware
        // next(err);
        errorHandler(err, req, res, next);
    }
}