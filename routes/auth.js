var express = require('express');
var router = express.Router();
const { validateRequest } = require('../middleware/globalMiddleware');
const roleController = require('../controllers/Auth/roleController');
const permissionController = require('../controllers/Auth/permissionController');
const rolePermissionController = require('../controllers/Auth/rolePermissionController');
const authController = require('../controllers/Auth/authController');
const authMiddleware = require('../middleware/authMiddleware');

// Role 
/* GET role listing. */
router.get('/role/', roleController.get);
// POST role
router.post('/role/', roleController.validation('validate'), validateRequest, roleController.post);
// GET role find by PK
router.get('/role/:id', roleController.findById);
// UPDATE permission
router.patch('/role/:id', roleController.validation('validate'), validateRequest, roleController.update);
// // DELETE users
router.delete('/role/:id', roleController.delete);


// Permission 
/* GET permission listing. */
router.get('/permission/', permissionController.get);
// POST permission
router.post('/permission/', permissionController.validation('validate'), validateRequest, permissionController.post);
// GET permission find by PK
router.get('/permission/:id', permissionController.findById);
// UPDATE permission
router.patch('/permission/:id', permissionController.validation('validate'), validateRequest, permissionController.update);

// Role & Permission
router.get('/role-permission/', rolePermissionController.get)
router.get('/role-permission/:id', rolePermissionController.findById)

// Role & Permission
/* GET Role & permission listing. */
router.post('/login', authController.validation('validate'), validateRequest, authController.login);
router.get('/verify', authController.verify);


// AUTH 
module.exports = router;
