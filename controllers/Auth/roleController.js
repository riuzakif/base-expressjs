// Default const 
const { errorHandler, successHandler, validateRequest } = require('../../middleware/globalMiddleware');
const { body, check } = require('express-validator');
const model = require('../../models/index');
const { dynamicUpdateData, dynamicStoreData, dynamicCreateOrUpdate, dynamicCreateOrUpdatePromise} = require('../../middleware/CRUDMiddleware');
const { sequelize } = require("../../models");
const _ = require('lodash');

// Define the whitelist of allowed columns with aliases
const allowedColumns = [
    'name',
    'is_active'
];
const allowedColumnsPermission = [
    'PermissionId',
    'RoleId'
]

exports.validation = (method) => {
    switch (method) {
        case 'validate': {
            return [
                body('name', 'name doesn`t exists').exists(),
            ]
        }
    }
}

// Display list of all Authors.
exports.get = async (req, res, next) => {
    try {
        const record = await model.Role.findAll({});
        if (record.length !== 0) {
            // Set response data and message in res.locals
            res.locals.data = record;
            res.locals.message = 'Success';
        } else {
            // Set response data and message in res.locals
            res.locals.message = 'Data is empty';
            res.locals.statusCode = 404;
            res.locals.data = {};
        }

        // Call successHandler middleware function
        successHandler(req, res, next);
    } catch (err) {
        // Call errorHandler middleware function with error object
        console.log(err);
        errorHandler(err, req, res, next);
    }
};

exports.post = async (req, res, next) => {
    try {
        // dynamic crete or update
        const defaultWhere = {
            name: req.body.name
        };
        // dynamic req type (body, query, param)
        const data = req.body;
        let listPermission = data.permissions;
        // function
        let store = await dynamicCreateOrUpdate(model.Role, data, allowedColumns, defaultWhere)(req, res, next, true);

        try {
            var listErrors = [];

            for (const permission of listPermission) {
                // dynamic crete or update
                const defaultWhere = {
                    RoleId: store.id,
                    PermissionId: permission.PermissionId
                };

                const t = await sequelize.transaction();
                await dynamicCreateOrUpdatePromise(model.RolePermission, defaultWhere, allowedColumnsPermission, defaultWhere)
                    .then(async record => {
                        await t.commit();
                    })
                    .catch(async err => {
                        await t.rollback();
                        listErrors.push(err.message)
                        console.log('listerrors', listErrors);
                    });
            }
            if (listErrors.length > 0) {
                model.Role.destroy({
                    where: {
                        id: store.id
                    }
                })
                throw new Error(listErrors.join());
            }

            res.locals.message = 'Record data created successfully';
            res.locals.data = {
                data: store,
                listPermission: listPermission
            };
            successHandler(req, res, next);
        } catch (err) {
            errorHandler(err, req, res, next);
        }

    } catch (err) {
        // console.log(err);
        errorHandler(err, req, res, next);
    }
};

exports.findById = async (req, res, next) => {
    try {
        const record = await model.Role.findByPk(req.params.id);
        if (!record) {
            res.locals.message = 'Data not found';
            res.locals.data = {};
            res.locals.statusCode = 404;
        }
        res.locals.data = record;
        res.locals.message = 'Success';
        res.locals.statusCode = 200;

        successHandler(req, res, next);
    } catch (err) {
        errorHandler(err, req, res, next);
    }
}

exports.update = async (req, res, next) => {
    try {
        // Extract the ID of the data to update from the request parameters
        const id = req.params.id;
        // Update the data using the dynamicUpdateData middleware
        await dynamicUpdateData(model.Role, allowedColumns)(req, res, next);
    } catch (err) {
        // Pass any errors to the error handling middleware
        // next(err);
        errorHandler(err, req, res, next);
    }
}

exports.delete = async (req, res, next) => {
    try {
        const role = await model.Role.findByPk(req.params.id);
        if (!role) {

            await role.destroy();
            res.locals.data = role;
            res.locals.message = 'Record Not Found!';
            res.locals.statusCode = 404;

            successHandler(req, res, next);
        }
        await role.destroy();
        res.locals.data = role;
        res.locals.message = 'Delete Success';
        res.locals.statusCode = 200;
        successHandler(req, res, next);
    } catch (err) {
        errorHandler(err, req, res, next);
    }
}