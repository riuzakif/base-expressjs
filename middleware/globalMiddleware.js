// Import required modules
const { validationResult } = require('express-validator');
const CryptoJS = require("crypto-js");

// Define global error handling middleware function
function errorHandler(err, req, res, next) {
    console.error(err.stack);

    // Set status code and response format based on error type
    if (err) {
        if (process.env.ENV_TYPE == 'DEV' && err.message != 'jwt expired') {
            res.status(500).json({
                status: 'error',
                statusCode: 500,
                error: true,
                message: err.message,
                stack: err.stack
            });
        } else {
            res.status(500).json({
                status: 'error',
                statusCode: 500,
                error: true,
                message: err.message
            });
        }
    } else {
        res.status(505).json({
            status: 'error',
            statusCode: 505,
            error: true,
            message: 'Internal Server Error'
        });
    }
}

// Define global validation middleware function
function validateRequest(req, res, next) {
    const errors = validationResult(req);

    // If there are validation errors, return error response
    if (!errors.isEmpty()) {
        return res.status(400).json({
            status: 'error',
            statusCode: 400,
            error: true,
            message: 'Validation error',
            errors: errors.array()
        });
    } else {
        // If there are no validation errors, continue to next middleware
        next();
    }
}

// Define global success response middleware function
function successHandler(req, res, next) {
    // Set default status code
    // console.log(res);
    res.statusCode = res.locals.statusCode || 200;
    let error = false;
    let status = 'success';
    if (res.statusCode != 200) {
        error = true;
        status = 'info';
    }

    // Define custom response format
    const response = {
        status: status,
        statusCode: res.statusCode,
        error: error,
        message: res.locals.message || 'Request successful',
        data: res.locals.data || null
    };

    // Send response
    res.json(response);
}

function encryptData(data) {
    var Key = CryptoJS.enc.Utf8.parse(process.env.AES_KEY); // 1. Replace C by CryptoJS
    var IV = CryptoJS.enc.Utf8.parse(process.env.AES_IV);
    var encryptedText = CryptoJS.AES.encrypt(JSON.stringify(data), Key, {
        iv: IV,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return encryptedText.toString(); // 2. Use Base64 instead of Hex
}

function decryptData(encryptedData) {
    var Key = CryptoJS.enc.Utf8.parse(process.env.AES_KEY);
    var IV = CryptoJS.enc.Utf8.parse(process.env.AES_IV);
    var decryptedText = CryptoJS.AES.decrypt(encryptedData, Key, { // 4. Use decrypt instead of encrypt
        iv: IV,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return JSON.parse(decryptedText.toString(CryptoJS.enc.Utf8)); // 5. Use decryptedText instead of encryptedData
}

// Export middleware functions
module.exports = {
    errorHandler,
    validateRequest,
    successHandler,
    encryptData,
    decryptData
};