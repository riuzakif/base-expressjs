// Default const 
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');
const { errorHandler, successHandler, encryptData } = require('../../middleware/globalMiddleware');
const { body, check } = require('express-validator');
const model = require('../../models/index');

// Define the whitelist of allowed columns with aliases
const allowedColumns = [
    'name',
];

exports.validation = (method) => {
    switch (method) {
        case 'validate': {
            return [
                body('username', 'username doesn`t exists').exists(),
                body('password', 'password doesn`t exists').exists(),
            ]
        }
    }
}

exports.login = async (req, res, next) => {

    const { username, password } = req.body;
    try {
        // const user = await model.User.findOne({ username });
        const user = await model.User.findOne({
            attributes: ['id', 'password', 'username', 'name', 'email', 'role'],
            where: {
                username: username,
            },
            include: {
                model: model.Role,
                include: {
                    model: model.Permission,
                    attributes: ['permission', 'action', 'type']
                }
            }
        });
        if (!user) {
            return res.status(400).json({ message: 'Invalid username or password' });
        }
        // console.log(user.password, user);
        // return
        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(400).json({ message: 'Invalid username or password' });
        }

        const payload = {
            user: {
                id: user.id,
                username: user.username
            }
        };

        if (user && isMatch) {
            const token = jwt.sign(
                payload,
                process.env.JWT_SECRET,
                { expiresIn: '1h' }
            );

            const listPermission = [];
            for (const perm of user.Role.Permissions) {
                listPermission.push({
                    permission: perm.permission,
                    action: perm.action,
                    type: perm.type,
                })
            }

            // save user token
            var response = {
                token,
                userData: encryptData({
                    id: user.id,
                    role: user.role,
                    fullname: user.name,
                    username: user.username,
                    email: user.email,
                }),
                listPermission: encryptData(listPermission)
            }
            // res.locals.message = 'Auth success';
            // res.locals.statusCode = 200;
            // res.locals.data = response;
        }

        // successHandler(req, res, next);
        return res.status(200).json(response);
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
};

exports.verify = async (req, res, next) => {
    try {
        const token = req.headers["x-access-token"];
        if (!token) {
            // return res.status(403).send("A token is required for authentication");
            return res.status(403).json(error("Error", res.statusCode, 'A token is required for authentication'));
        }
        try {
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            // console.log(decoded);
            // return
            const user = await model.User.findOne({
                where: {
                    id: decoded.user.id,
                },
                include: {
                    model: model.Role,
                    include: {
                        model: model.Permission,
                        attributes: ['permission', 'action', 'type']
                    }
                }
            });
            if (user === null) {
                throw new Error('User Not Found');
            }
            const listPermission = [];
            for (const perm of user.Role.Permissions) {
                listPermission.push({
                    permission: perm.permission,
                    action: perm.action,
                    type: perm.type,
                })
            }
            var response = {
                userData: encryptData({
                    id: user.id,
                    role: user.role,
                    fullname: user.name,
                    username: user.username,
                    email: user.email,
                }),
                listPermission: encryptData(listPermission)
            }
            // res.locals.message = 'Verify success';
            // res.locals.statusCode = 200;
            // res.locals.data = response;
            return res.status(200).json(response);
            // return successHandler(req, res, next);
        } catch (err) {
            // return res.status(401).json(error("Error", res.statusCode, err.message));
            return errorHandler(err, req, res, next)
        }
    } catch (err) {
        return errorHandler(err, req, res, next)
        // return res.status(401).json(error("Error", res.statusCode, 'x-access-token not found'));
    }
}