const express = require('express');
const router = express.Router();
const { errorHandler, successHandler, encryptData, decryptData } = require('../middleware/globalMiddleware');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/decrypt', function (req, res, next) {
  try {
    const {
      decrypt
    } = req.body;
    const record = decryptData(decrypt);

    res.locals.message = 'Auth success';
    res.locals.statusCode = 200;
    res.locals.data = record;
    successHandler(req, res, next);
  } catch (err) {
    return errorHandler(err, req, res, next)
  }

});

module.exports = router;
