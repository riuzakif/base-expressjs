const { errorHandler, successHandler, validateRequest } = require('../middleware/globalMiddleware');

// FUNCTION FOR CRUD 
// Define the function
function dynamicUpdateData(model, allowedColumns) {
    return async function (req, res, next) {
        // console.log(res);
        try {
            const id = req.params.id;
            const body = req.body;

            // Create an object that maps the aliases to the actual column names
            const columnMapping = {};

            // Loop through the allowed columns and create the mapping object
            for (const column of allowedColumns) {
                if (typeof column === 'object' && column !== null) {
                    // If the column is an object, it has an alias and a name
                    columnMapping[column.alias] = column.name;
                } else {
                    // If the column is a string, it has no alias
                    columnMapping[column] = column;
                }
            }
            // console.log(columnMapping);
            // Create an object that will hold the updated data
            const updatedData = {};

            // Loop through the request body and copy over the allowed columns
            for (const [key, value] of Object.entries(body)) {
                if (columnMapping[key]) {
                    updatedData[columnMapping[key]] = value;
                }
            }

            // Update the data using Sequelize
            const [rowsAffected, updatedRows] = await model.update(updatedData, {
                where: {
                    id: id,
                },
                returning: true, // <-- Set the "returning" option to true
            });

            // Check if any rows were affected
            if (rowsAffected === 0) {
                // If no rows were affected, throw an error
                // throw new Error('Data not found or not updated');
                // return res.status(404).json({ message: 'Data not found' });
                res.locals.message = 'Data not found or not updated';
                return successHandler(req, res, next);
            }

            // Return a response to the client with the updated data
            // res.status(200).json({ data: updatedData[0] });
            // res.statusCode = 200;
            res.locals.message = 'Update record success';
            res.locals.data = updatedRows;
            successHandler(req, res, next);
        } catch (err) {
            // Pass any errors to the error handling middleware
            // next(err);
            errorHandler(err, req, res, next);

        }
    };
}


// dynamic store 
function dynamicStoreData(model, allowedColumns) {
    return async function (req, res, next) {
        try {
            const body = req.body;
            // Create an object that maps the aliases to the actual column names
            const columnMapping = {};
            // Loop through the allowed columns and create the mapping object
            for (const column of allowedColumns) {
                if (typeof column === 'object' && column !== null) {
                    // If the column is an object, it has an alias and a name
                    columnMapping[column.alias] = column.name;
                } else {
                    // If the column is a string, it has no alias
                    columnMapping[column] = column;
                }
            }
            // Create an object that will hold the data to be stored
            const newData = {};
            // Loop through the request body and copy over the allowed columns
            for (const [key, value] of Object.entries(body)) {
                if (columnMapping[key]) {
                    newData[columnMapping[key]] = value;
                }
            }
            // Store the data using Sequelize
            const createdData = await model.create(newData);
            // Send the created data back to the client as a response
            res.locals.message = 'Record data created successfully';
            res.locals.data = createdData;
            successHandler(req, res, next);
        } catch (err) {
            // Pass any errors to the error handling middleware
            // next(err);
            errorHandler(err, req, res, next);
        }
    };
}

// dynamic create or update 
function dynamicCreateOrUpdate(model, data, allowedColumns, defaultWhere = {}, whereOverrides = {}) {
    return async function (req, res, next) {
        try {
            // Create an object that maps the aliases to the actual column names
            const columnMapping = {};
            // Loop through the allowed columns and create the mapping object
            for (const column of allowedColumns) {
                if (typeof column === 'object' && column !== null) {
                    // If the column is an object, it has an alias and a name
                    columnMapping[column.alias] = column.name;
                } else {
                    // If the column is a string, it has no alias
                    columnMapping[column] = column;
                }
            }
            // Create an object that will hold the data to be stored
            const newData = {};
            // Loop through the request body and copy over the allowed columns
            for (const [key, value] of Object.entries(data)) {
                if (columnMapping[key]) {
                    newData[columnMapping[key]] = value;
                }
            }

            // Merge default where clause with overrides
            const where = { ...defaultWhere, ...whereOverrides };

            // Check if record already exists
            const existingRecord = await model.findOne({
                where,
            });

            if (existingRecord) {
                // Update existing record
                const updatedRows = await existingRecord.update(newData);
                res.locals.message = 'Record data updated successfully';
                res.locals.data = updatedRows;
                successHandler(req, res, next);
                // return updatedRecord;
            } else {
                // Create new record
                const createdRecord = await model.create(newData);
                res.locals.message = 'Record data created successfully';
                res.locals.data = createdRecord;
                successHandler(req, res, next);
                // return createdRecord;
            }
        } catch (err) {
            // Pass any errors to the error handling middleware
            // next(err);
            errorHandler(err, req, res, next);
        }
    }
}


// dynamic create or update Promise
function dynamicCreateOrUpdatePromise(model, data, allowedColumns, defaultWhere = {}, whereOverrides = {}) {
    return new Promise(async (resolve, reject) => {
        try {
            // Create an object that maps the aliases to the actual column names
            const columnMapping = {};
            // Loop through the allowed columns and create the mapping object
            for (const column of allowedColumns) {
                if (typeof column === 'object' && column !== null) {
                    // If the column is an object, it has an alias and a name
                    columnMapping[column.alias] = column.name;
                } else {
                    // If the column is a string, it has no alias
                    columnMapping[column] = column;
                }
            }
            // Create an object that will hold the data to be stored
            const newData = {};
            // Loop through the request body and copy over the allowed columns
            for (const [key, value] of Object.entries(data)) {
                if (columnMapping[key]) {
                    newData[columnMapping[key]] = value;
                }
            }

            // Merge default where clause with overrides
            const where = { ...defaultWhere, ...whereOverrides };

            // Check if record already exists
            const existingRecord = await model.findOne({
                where,
            });

            if (existingRecord) {
                // Update existing record
                const updatedRows = await existingRecord.update(newData);
                resolve(updatedRows);
            } else {
                // Create new record
                const createdRecord = await model.create(newData);
                resolve(createdRecord);
            }
        } catch (err) {
            reject(err);
        }
    });
}

// Export middleware functions
module.exports = {
    dynamicUpdateData,
    dynamicStoreData,
    dynamicCreateOrUpdate,
    dynamicCreateOrUpdatePromise
};